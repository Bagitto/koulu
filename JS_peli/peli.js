console.log("bägi is here");  // testaan, että ohjelma lähtee käyntiin
// määritellään muuttuja, vakio, pystyy aina samana
// hae html-dokementista canvaasi-id:llä oleva elementti
const canvas = document.getElementById('canvaasi');
// tehdään 2D
const ctx = canvas.getContext('2d');
// let myös määrittelee muuttujan, mutta sen avaa voi muuttaa, var 
let raf;
let inputStates = {} // näppäintenpainallukset tänne  
window.addEventListener('keydown', function(event){
  console.log(event.key);
  if (event.key == "ArrowRight") {
  console.log ("Oikea nuoli painattu")
    inputStates.right = true;
  }
  if (event.key == "ArrowLeft") {
    console.log ("vasen nuoli painattu")
      inputStates.left = true;
    }

}, false);
window.addEventListener('keyup', function(event){
  console.log(event.key);
  if (event.key == "ArrowRight") {
  console.log ("Oikea nuoli painattu")
    inputStates.right = false;
  }
  if (event.key == "ArrowLeft") {
    console.log ("vasen nuoli painattu")
      inputStates.left = false;
    }

}, false);

// Javascriptin opjektin määrittely, omenaisuuksien arvoja voi muuttaa 
const ball = {
  x: 100,
  y: 100,
  vx: 5, // nopeus ja suunta
  vy: 2,
  radius: 25, 
  color: '#e84393',
  draw() {
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fillStyle = this.color;
    ctx.fill();
  }
}; // tähän loppuu tämän opjektin määrittely


const maila = {
  x: 250,
  y: 385,
  vx: 5, // nopeus ja suunta
  leveys: 100,
  korkeus: 15, 
  color: '#2c3e50',
  draw() {
  
    ctx.fillStyle = this.color;
    ctx.fillRect(this.x, this.y, this.leveys, this.korkeus);

  }

};

// peliluuppi
// gunktio on koodinpätkä, jotka suoretaan voin käskytä
function draw() {

  // thjennetään kaikki 
  ctx.clearRect(0,0, canvas.width, canvas.height);
  // piirrä pelimaailma
  ball.draw();

  maila.draw();

  // liikuta pelimaailma, liikuta kaikki liikuvat opjekti 
  ball.x += ball.vx;
  ball.y += ball.vy;

  //liikuta maila oikealle
  if (inputStates.right) { // moikea nuoli painattu
    maila.x = maila.x + maila.vx
  }
  if (inputStates.left) { // vasen nuoli painattu
    maila.x = maila.x - maila.vx;
  }
 
  if (ball.y + ball.radius > canvas.height ||
      ball.y - ball.radius < 0) {
    ball.vy = -ball.vy;
  }

  // törmäsikö pallo seinään 
  if (ball.x + ball.radius > canvas.width ||
      ball.x - ball.radius < 0) {
    ball.vx = -ball.vx;
  }
  

  // törmääkö maila seinään 
  // Jos maila törmää seinää,
  //niin palautetaan se takaisin pelilaudalle
  if (maila.x < 0){
    maila.x = 0;
  }

  if (maila.x + maila.leveys> canvas.width) {
    maila.x = canvas.width - maila.leveys;
  }

  // odota 
  raf = window.requestAnimationFrame(draw);
  {
  var testX=ball.x;
  var testY=ball.y;
  
  if(testX < maila.x)  testX=maila.x;
  else if (testX > (maila.x + maila.leveys)) Test=test =(maila.x + maila.leveys)
  if (testY < maila.y) testY = maila.y;
  else if (testY > (maila.y + maila.korkeus)) testY = (maila.y + maila.korkeus)
  
  
  var distX = ball.x - testX;
  var distY = ball.y - testY;
  var dista = Math.sqrt((distX*distX) + (distY * distY));
  
  if (dista <=ball.radius) {
    if(ball.x >= maila.x && ball.x <= (maila.x + maila.leveys)) {
      ball.vy *= -1;
    }
  
  } else if (ball.y >= maila.y && ball.y <= (maila.y + maila.korkeus)) {
    ball.vx *= -1;
       }
  }
}

draw(); // suorita tämä funktio
 // funktio määritys loppuu tähän